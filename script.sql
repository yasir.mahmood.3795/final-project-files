USE [master]

GO

DROP DATABASE IF EXISTS [ProjectManagementToolDatabase];

GO

CREATE DATABASE [ProjectManagementToolDatabase];

GO

USE [ProjectManagementToolDatabase];

GO

CREATE TABLE Lookup (
    ID INT PRIMARY KEY,
    Category VARCHAR(50),
    Value VARCHAR(50)
);

INSERT INTO Lookup VALUES (1, 'Gender', 'Male');
INSERT INTO Lookup VALUES (2, 'Gender', 'Female');

INSERT INTO Lookup VALUES (3, 'UserType', 'HR Manager');
INSERT INTO Lookup VALUES (4, 'UserType', 'Project Manager');
INSERT INTO Lookup VALUES (5, 'UserType', 'Employee');


INSERT INTO Lookup VALUES (6, 'AttendanceStatus', 'Present');
INSERT INTO Lookup VALUES (7, 'AttendanceStatus', 'Absent');

INSERT INTO Lookup VALUES (8, 'LeaveType', 'Sick Leave');
INSERT INTO Lookup VALUES (9, 'LeaveType', 'Vacation Leave');

INSERT INTO Lookup VALUES (10, 'LeaveStatus', 'Pending');
INSERT INTO Lookup VALUES (11, 'LeaveStatus', 'Approved');
INSERT INTO Lookup VALUES (12, 'LeaveStatus', 'Rejected');

INSERT INTO Lookup VALUES (13, 'ProjectStatus', 'Not Started');
INSERT INTO Lookup VALUES (14, 'ProjectStatus', 'In Progress');
INSERT INTO Lookup VALUES (15, 'ProjectStatus', 'Completed');

INSERT INTO Lookup VALUES (16, 'ResourceType', 'Hardware');
INSERT INTO Lookup VALUES (17, 'ResourceType', 'Software');

INSERT INTO Lookup VALUES (18, 'TaskStatus', 'Not Started');
INSERT INTO Lookup VALUES (19, 'TaskStatus', 'In Progress');
INSERT INTO Lookup VALUES (20, 'TaskStatus', 'Completed');

INSERT INTO Lookup VALUES (21, 'TaskPriority', 'Low');
INSERT INTO Lookup VALUES (22, 'TaskPriority', 'Medium');
INSERT INTO Lookup VALUES (23, 'TaskPriority', 'High');


INSERT INTO Lookup VALUES
(24, 'Role', 'Project Manager'),
(25, 'Role', 'Business Analyst'),
(26, 'Role', 'Software Developer'),
(27, 'Role', 'Quality Assurance (QA) Engineer'),
(28, 'Role', 'UX/UI Designer'),
(29, 'Role', 'Technical Writer'),
(30, 'Role', 'DevOps Engineer'),
(31, 'Role', 'Database Administrator'),
(32, 'Role', 'Security Engineer'),
(33, 'Role', 'Support Engineer');


CREATE TABLE [Users](
    [UserID] INT IDENTITY(1,1) PRIMARY KEY,
    [FirstName] VARCHAR(50) NOT NULL,
    [LastName] VARCHAR(50) NULL,
    [DateOfBirth] DATE NULL,
    [Gender] INT NULL,
	[Email] VARCHAR(35),
    [PhoneNumber] VARCHAR(20) NULL,
    [UserType] INT,
    FOREIGN KEY (Gender) REFERENCES Lookup(ID),
    FOREIGN KEY (UserType) REFERENCES Lookup(ID)
);



CREATE TABLE[LeaveRequest](
	[LeaveID] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[EmployeeID] [int] NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[LeaveType] [int] NULL,
	[LeaveStatus] [int] NULL,
	[ManagerID] [int] NULL,
	FOREIGN KEY (EmployeeID) REFERENCES Users(UserID),
	FOREIGN KEY (LeaveType) REFERENCES Lookup(ID),
	FOREIGN KEY (LeaveStatus) REFERENCES Lookup(ID),
	FOREIGN KEY (ManagerID) REFERENCES Users(UserID),
);

CREATE TABLE [Project](
	[ProjectID] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[ProjectName] [varchar](50) NOT NULL,
	[ProjectDescription] [varchar](100) NULL, 
	[StartDate] [date] NULL,
	[EndDate] [date] NOT NULL,
	[ProjectStatus] [int] NULL,
	[ManagerID] [int] NULL,
	FOREIGN KEY (ProjectStatus) REFERENCES Lookup(ID),
	FOREIGN KEY (ManagerID) REFERENCES Users(UserID),
);

CREATE TABLE [ProjectTeam](
	[ProjectTeamID] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[ProjectID] [int] NULL,
	[AssignmentDate] [date] NULL,
	FOREIGN KEY (ProjectID) REFERENCES Project(ProjectID)
)

CREATE TABLE [ProjectTeamMember](
	[ProjectTeamID] [int] NULL ,
	[EmployeeID] [int] NULL,
	[Role] [int] NULL,
	FOREIGN KEY (Role) REFERENCES Lookup(ID),
	FOREIGN KEY (ProjectTeamID) REFERENCES ProjectTeam(ProjectTeamID),
	FOREIGN KEY (EmployeeID) REFERENCES Users(UserID),
);

CREATE TABLE [Resource](
	[ResourceID] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[ResourceName] [varchar](50) NOT NULL,
	[ResourceType] [int] NULL,
	[Cost] [int] NULL,
	FOREIGN KEY (ResourceType) REFERENCES Lookup(ID),
)


CREATE TABLE [Task](
	[TaskID] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[ProjectID] [int] NULL,
	[TaskName] [varchar](50) NULL,
	[TaskDescription] [varchar](200) NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[TaskStatus] [int] NULL,
	[TaskPriority] [int] NULL,
	[AssignedTo] [int] NULL,
	FOREIGN KEY (ProjectID) REFERENCES Project(ProjectID),
	FOREIGN KEY (TaskStatus) REFERENCES Lookup(ID),
	FOREIGN KEY (TaskPriority) REFERENCES Lookup(ID),
	FOREIGN KEY (AssignedTo) REFERENCES Users(UserID),
);


CREATE TABLE [TaskResource](
	[ID] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[TaskID] [int] NULL,
	[ResourceID] [int] NULL,
	FOREIGN KEY (TaskID) REFERENCES Task(TaskID),
	FOREIGN KEY (ResourceID) REFERENCES Resource(ResourceID)
);

CREATE TABLE [Credentials] (
	[CredentialID] INT IDENTITY(1,1) PRIMARY KEY,
	[UserID] INT,
	[Username] VARCHAR(50),
	[Password] VARCHAR(100),
	FOREIGN KEY (UserID) REFERENCES Users(UserID)
);


CREATE TABLE [Attendance](
	[AttendanceID] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY, 
	[EmployeeID] [int] NOT NULL,
	[AttendanceDate] [date] NOT NULL,
	[AttendanceStatus] [int] NOT NULL,
	[ManagerID] [int] NOT NULL,
	FOREIGN KEY (EmployeeID) REFERENCES Users(UserID),
	FOREIGN KEY (AttendanceStatus) REFERENCES Lookup(ID),
	FOREIGN KEY (ManagerID) REFERENCES Users(UserID)
);


INSERT INTO [Users] (FirstName, LastName, DateOfBirth, Gender, Email, PhoneNumber, UserType)
	VALUES ('Affan', 'Chaudhary', '2002-10-01', 1, 'affan@gmail.com', '03234545530', 4);



INSERT INTO [Users] (FirstName, LastName, DateOfBirth, Gender, Email, PhoneNumber, UserType)
VALUES ('Shaheen', 'Bey', '2001-09-10', 1, 'shaheen@parwaz.com', '03234540530', 3);



INSERT INTO [Users] (FirstName, LastName, DateOfBirth, Gender, Email, PhoneNumber, UserType)
VALUES ('Fahad', 'Khan', '2000-03-20', 1, 'fahad@isi.gov.pk', '03554540530', 5);



